package asad.love.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class watchActivity extends AppCompatActivity {
    Button btnget,rateus;
    TextView tvSplash,tvSubSplash;
    Animation atgo,btngos,tvshow;
    ImageView splash;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch);
        btnget = findViewById(R.id.btn_get);
        tvSplash = findViewById(R.id.tv_splash);
        tvSubSplash = findViewById(R.id.text_subSplash);
        splash = findViewById(R.id.ivSplash);
        rateus = findViewById(R.id.rate_us);


        //load animation
        atgo = AnimationUtils.loadAnimation(this,R.anim.atg);
        btngos = AnimationUtils.loadAnimation(this,R.anim.btngo);
        tvshow = AnimationUtils.loadAnimation(this,R.anim.tvgo);
        //apply animation in image.
        splash.startAnimation(atgo);
        //apply on button.
        btnget.setAnimation(btngos);
        tvSplash.setAnimation(tvshow);
        tvSubSplash.setAnimation(tvshow);

        btnget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(watchActivity.this,Stop_watch_Activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
            }
        });

        rateus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent  = new Intent(watchActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
