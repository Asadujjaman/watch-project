package asad.love.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;

public class Stop_watch_Activity extends AppCompatActivity {
    Button btnstart,btnstop,btnpause;
    ImageView icanchor;
    Animation roundingalone;
    Chronometer timer;
    private long pauseoffset;
    private boolean running;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_watch);
        btnstart = findViewById(R.id.btn_start);
        icanchor = findViewById(R.id.ic_Anchor);
        timer = findViewById(R.id.timerHere);
        btnstop = findViewById(R.id.btn_finish);
        btnpause = findViewById(R.id.btn_pause);

        //new things only for timers.
        //timer.setFormat("Time: %s");
        timer.setBase(SystemClock.elapsedRealtime());

        //this line for set a time which is true the timer is automatically reset.its optional.
       /* timer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                if ((SystemClock.elapsedRealtime() - chronometer.getBase()) >= 10000 ){
                    chronometer.setBase(SystemClock.elapsedRealtime());
                }
            }
        }); */




        //create option animation.
       // btnstop.setAlpha(0);

        //animation setup.
        roundingalone = AnimationUtils.loadAnimation(this,R.anim.roundingalone);

        btnstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btnpause.animate().alpha(1).translationY(-130).setDuration(300).start();
                btnstop.animate().alpha(1).translationY(-130).setDuration(300).start();
                btnstart.animate().alpha(0).setDuration(300).start();
                //time set here
                icanchor.startAnimation(roundingalone);


                if (!running){
                    timer.setBase(SystemClock.elapsedRealtime()  - pauseoffset);
                    timer.start();
                    running = true;
                }


            }
        });

        btnpause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnstart.animate().alpha(1).translationY(-70).setDuration(300).start();
                btnstop.animate().alpha(1).translationY(-130).setDuration(300).start();
                btnpause.animate().alpha(0).setDuration(300).start();
                icanchor.clearAnimation();
                if (running){
                    timer.stop();
                    pauseoffset = SystemClock.elapsedRealtime() - timer.getBase();
                    running = false;
                   // icanchor.startAnimation(roundingalone);
                }

            }
        });

        btnstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                icanchor.clearAnimation();
                btnstart.animate().alpha(1).translationY(-5).setDuration(300).start();
                btnpause.animate().alpha(1).translationY(-5).setDuration(300).start();
                btnstop.animate().alpha(0).setDuration(300).start();

                timer.setBase(SystemClock.elapsedRealtime());
                pauseoffset = 0;

            }
        });
    }
}
