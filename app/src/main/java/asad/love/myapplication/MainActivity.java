package asad.love.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView titlerate, resultrate;
    Button feedback_button,watch;
    RatingBar rateStars;
    ImageView imageView;
    String answerValue;

    Animation charanim,btt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        titlerate = findViewById(R.id.title_rate);
        resultrate = findViewById(R.id.result_rate);
        feedback_button = findViewById(R.id.feed_back);
        rateStars = findViewById(R.id.rating_bar);
        imageView = findViewById(R.id.charPlace);
        watch = findViewById(R.id.stop_watch);

        //load animation
        charanim = AnimationUtils.loadAnimation(this,R.anim.charanim);
        btt = AnimationUtils.loadAnimation(this,R.anim.btt);

        //give animation
        imageView.startAnimation(charanim);
        feedback_button.startAnimation(btt);

       /* Typeface mRegular = Typeface.createFromAsset(getAssets(),"fontfamily_name_regular");
        Typeface mMedium = Typeface.createFromAsset(getAssets(),"fontfamily_name_medium");
        titlerate.setTypeface(fontfamily_name_regular);*/ //this is for changing font family by using java code.

        watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,watchActivity.class);
                startActivity(intent);
            }
        });


        //give condition
        rateStars.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                answerValue = String.valueOf((int) (rateStars.getRating()));
                if (answerValue.equals("1")){
                    imageView.setImageResource(R.drawable.emoji_cry);
                    //give animation
                    imageView.startAnimation(charanim);
                    feedback_button.startAnimation(btt);
                    resultrate.setText("Sorry!!!!!");
                }else if (answerValue.equals("2")){
                    imageView.setImageResource(R.drawable.emoji_cry);
                    //give animation
                    imageView.startAnimation(charanim);
                    feedback_button.startAnimation(btt);
                    resultrate.setText("Sorry!!!!!");
                }else if (answerValue.equals("3")){
                    imageView.setImageResource(R.drawable.emoji_sad);
                    //give animation
                    imageView.startAnimation(charanim);
                    feedback_button.startAnimation(btt);
                    resultrate.setText("It's Ok.");
                }else if (answerValue.equals("4")){
                    imageView.setImageResource(R.drawable.emoji_sad);
                    //give animation
                    imageView.startAnimation(charanim);
                    feedback_button.startAnimation(btt);
                    resultrate.setText("It's Ok.");
                }else if (answerValue.equals("5")){
                    imageView.setImageResource(R.drawable.emoji_fulhappy);
                    //give animation
                    imageView.startAnimation(charanim);
                    feedback_button.startAnimation(btt);
                    resultrate.setText("Thank you boss.");
                }else {
                    Toast.makeText(MainActivity.this, "No Rating you give us", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }
}
